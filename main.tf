output "data" {
  value = "${var.data}"
}
output "host" {
  value = "${var.host}"
}
output "ipaddr" {
  value = "${var.ipaddr}"
}