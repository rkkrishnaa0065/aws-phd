variable "ipaddr" {
  default = "127.0.0.1"
}

variable "host" {
  default = "localhost"
}

variable "data" {
  default = "Linux"
}